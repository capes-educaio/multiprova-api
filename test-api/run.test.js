const { setLoginData } = require('./setTestData')
const { describeModel } = require('./describeModel')

const { usuario } = require('./modelos/usuario')
const { questao } = require('./modelos/questao')
const { questaoCedida } = require('./modelos/questaoCedida')
const { prova } = require('./modelos/prova')
const { instanciamento } = require('./modelos/instanciamento')
const { turma } = require('./modelos/turma')
const { notificacao } = require('./modelos/notificacao')

before(done => {
  setTimeout(() => done(), 1000)
})

describe('Setar informações iniciais', () => {
  setLoginData('docente')
  setLoginData('professor')
  setLoginData('discente')
  setLoginData('admin')
  setLoginData('super')
  setLoginData('gestor')
})

describeModel(usuario)
describeModel(questao)
describeModel(questaoCedida)
describeModel(prova)
describeModel(instanciamento)
describeModel(turma)
describeModel(notificacao)
