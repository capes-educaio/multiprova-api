export const enumStatusInstanciamento = {
  naoIniciada: 'Não iniciada',
  emExecucao: 'Em execução',
  concluida: 'Concluída',
}
