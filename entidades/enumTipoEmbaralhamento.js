export const enumTipoEmbaralhamento = {
  naoEmbaralhar: 0,
  embaralhar: 1,
  embaralharComRestricao: 2,
}
