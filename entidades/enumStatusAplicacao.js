export const enumStatusAplicacao = {
  elaboracao: 'Em elaboração',
  prontaParaAplicacao: 'Pronta para aplicação',
  emAplicacao: 'Em aplicação',
  aplicada: 'Aplicada',
  corrigida: 'Corrigida',
  finalizada: 'Finalizada',
  consolidada: 'Consolidada',
}
