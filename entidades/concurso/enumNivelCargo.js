export const enumNivelCargo = {
  indefinido: 'Indefinido',
  ensinoFundamental: 'Ensino Fundamental',
  ensinoMedio: 'Ensino Medio',
  ensinoSuperior: 'Ensino Superior',
  mestrado: 'Mestrado',
  doutorado: 'Doutorado',
}
