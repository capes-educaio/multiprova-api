export const enumStatusQuestao = {
  elaboracao: { id: 'el', texto: 'Em elaboração' },
  primeiraRevisaoPedagogica: { id: 'pp', texto: 'Primeira revisão pedagógica' },
  validacaoDaRevisaoPedagogica: {
    id: 'vp',
    texto: 'Validação da revisão pedagógica (elaborador)',
  },
  revisaoDeConteudo: { id: 'rc', texto: 'Revisão de conteúdo' },
  revisaoDeLinguagem: { id: 'rl', texto: 'Revisão de linguagem' },
  validacaoDaRevisaoDeLinguagem: {
    id: 'vl',
    texto: 'Validação da revisão de linguagem',
  },
  liberadoParaImpressao: { id: 'li', texto: 'Liberado para impressão' },
}
