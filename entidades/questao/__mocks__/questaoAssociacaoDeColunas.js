export const questaoAssociacaoDeColunas = {
  dataCadastro: '2019-01-08T16:35:00.180Z',
  dataUltimaAlteracao: '2019-01-09T19:35:00.180Z',
  enunciado:
    'Considerando as principais operações de calculo diferencial e integral, relacione as afirmações apresentadas no lado diretio com os termos mostrados no lado esquerdo, apresentados abaixo.',
  tipo: 'associacaoDeColunas',
  associacaoDeColunas: {
    colunaA: [
      {
        rotulo: 1,
        conteudo: 'Derivada',
      },
      {
        rotulo: 2,
        conteudo: 'Integral',
      },
      {
        rotulo: 3,
        conteudo: 'Gradiente',
      },
      {
        rotulo: 4,
        conteudo: 'Rotacional',
      },
      {
        rotulo: 5,
        conteudo: 'Laplaciano',
      },
    ],
    colunaB: [
      {
        rotulo: 5,
        conteudo: 'Divergente do gradiente de uma função',
      },
      {
        rotulo: 1,
        conteudo: 'Taxa de variação instantânea da função.',
      },
      {
        rotulo: 3,
        conteudo: 'Determinar a direção de máximo crescimento.',
      },
      {
        rotulo: 2,
        conteudo: 'Calcular a área de uma curva qualquer.',
      },
      {
        rotulo: 4,
        conteudo:
          'Calcular, em uma superfície infinitesimal, o quanto os vetores de um campo vetorial se afastam ou se aproximam de um vetor normal a esta superfície.',
      },
    ],
  },
}
