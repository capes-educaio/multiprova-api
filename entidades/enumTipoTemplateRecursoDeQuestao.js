export const tiposTemplateRecursoDeQuestao = Object.freeze({
  comperve: 'comperve/recursoDeQuestao',
  geral: 'geral/recursoDeQuestao',
})

export const enumTipoTemplateRecursoDeQuestao = Object.freeze({
  recursoDeQuestaoComperve: tiposTemplateRecursoDeQuestao.comperve,
  recursoDeQuestaoGeral: tiposTemplateRecursoDeQuestao.geral,
})
