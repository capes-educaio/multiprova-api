const provas = []

const prova1 = [
  {
    id: '972a13e7-d043-4ecc-98ad-53b99cc09aad',
    descricao: 'PROVA DO NÍVEL 1 DO ANO DE 2018',
    titulo: 'PROVA DE MATEMÁTICA',
    assunto: 'LOGARÍTMOS',
    dataAplicacao: '2018-06-19',
    instituicao: 'UFRN',
    professor: 'Arimatéia',
    questoes: [],
  },
]
provas.push(prova1)

const prova2 = [
  {
    id: '43294da3-a715-4665-b532-05c8256b2e44',
    descricao: 'PROVA DO NÍVEL 2 DO ANO DE 2018',
    titulo: 'PROVA DE FISICA',
    assunto: 'ELETRICIDADE',
    dataAplicacao: '2018-06-19',
    instituicao: 'UFRN',
    professor: 'Arimatéia',
    questoes: [],
  },
]
provas.push(prova2)

const prova3 = [
  {
    id: '117049ba-f3b0-4f4f-9a48-c4e1a3feb733',
    descricao: 'PROVA DO NÍVEL 3 DO ANO DE 2018',
    titulo: 'PROVA DE PORTUGUÊS',
    assunto: 'VERBO',
    dataAplicacao: '2018-06-19',
    instituicao: 'UFRN',
    professor: 'Arimatéia',
    questoes: [],
  },
]
provas.push(prova3)

const prova4 = [
  {
    id: 'f4d6a61a-2dfd-4725-8fa9-04cdc2766ea0',
    descricao: 'PROVA DO NÍVEL 4 DO ANO DE 2018',
    titulo: 'PROVA DE GEOGRAFIA',
    assunto: 'PAISES',
    dataAplicacao: '2018-06-19',
    instituicao: 'UFRN',
    professor: 'Arimatéia',
    questoes: [],
  },
]
provas.push(prova4)

export default provas
