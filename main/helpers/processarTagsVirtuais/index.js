export { substituirTagsVirtuaisPorTagIds } from './substituirTagsVirtuaisPorTagIds'
export { processarTagsVirtuais } from './processarTagsVirtuais'
export { processarTagsOnSave } from './processarTagsOnSave'
