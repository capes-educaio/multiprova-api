import app from 'server/server'

import { statusInstanciamentoCaderno } from 'entidades/caderno/cadernoNoConcurso/contratoCadernoInstanciamento'
import { enumStatusExportacaoQuestoes } from 'entidades/concurso/enumStatusExportacaoQuestoes'
import { patchInstanciamento } from './patchInstanciamento'

const excluirInstanciamentos = async ({ versao }) => {
  const instancias = await app.models.InstanciamentoConcursos.find({ where: { versao } })
  for (let index in instancias) await instancias[index].destroy()
  return instancias.length
}

export const excluirVersaoImagensQuestoes = concurso => {
  if (concurso.exportacaoQuestoes.status === enumStatusExportacaoQuestoes.pendente) return
  console.log('Excluindo a versão das imagens das questões do concurso')
  concurso.exportacaoQuestoes = {
    status: enumStatusExportacaoQuestoes.pendente,
    impressaoId: '',
    data: new Date().toISOString(),
  }
  return concurso.save()
}

export const excluirInstanciamentoCaderno = async (instancia, cadernoId, versao) => {
  const { concurso, mudouStatus } = await patchInstanciamento({
    novasPropriedades: {
      status: statusInstanciamentoCaderno.excluido,
      dataExluido: new Date().toISOString(),
    },
  })({ versao, instancia, cadernoId })
  const numeroDeInstanciamentosExcluidos = await excluirInstanciamentos({ versao })
  await excluirVersaoImagensQuestoes(instancia)
  return [mudouStatus, numeroDeInstanciamentosExcluidos, concurso.toObject()]
}
