export const metodosParaDesativar = {
  standard: [
    'prototype.patchAttributes',
    'patchOrCreate',
    'replaceOrCreate',
    'upsertWithWhere',
    'updateAll',
    'findOne',
    'replaceById',
    'exists',
    'createChangeStream',
  ],
  candidato: ['prototype.__get__candidato'],
  usuario: ['prototype.__get__usuario'],
}
