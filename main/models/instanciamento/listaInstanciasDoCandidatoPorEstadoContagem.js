import { models } from 'server/server'

export const listaInstanciasDoCandidatoPorEstadoContagem = (candidatoId, estado, cb) => {
  try {
    const { Instanciamento } = models
    const where = {
      candidatoId: candidatoId,
      tipo: 'prova',
      isDeleted: false,
    }
    Instanciamento.find(where, function(err, instanciamentos) {
      if (err) return cb(err)

      if (estado === 'abertas') instanciamentos = instanciamentos.filter(filtroDeEstadoAbertas)
      if (estado === 'agendadas') instanciamentos = instanciamentos.filter(filtroDeEstadoAgendadas)
      if (estado === 'finalizadas') instanciamentos = instanciamentos.filter(filtroDeEstadoFinalizadas)

      cb(null, instanciamentos.length)
    })
  } catch (e) {
    cb(new Error())
  }
}

const filtroDeEstadoAbertas = instancia => {
  if (!instancia || !instancia.virtual) return

  const agora = new Date()
  const dataInicioProva = new Date(instancia.virtual.dataInicioProva)
  const dataTerminoProva = new Date(instancia.virtual.dataTerminoProva)
  const status = instancia.status

  if (dataInicioProva < agora && dataTerminoProva > agora && status !== 'Concluída') {
    return instancia
  }
}

const filtroDeEstadoAgendadas = instancia => {
  if (!instancia || !instancia.virtual) return
  const agora = new Date()
  const dataInicioProva = new Date(instancia.virtual.dataInicioProva)

  if (dataInicioProva > agora) {
    return instancia
  }
}

const filtroDeEstadoFinalizadas = instancia => {
  if (!instancia || !instancia.virtual) return
  const agora = new Date()
  const dataTerminoProva = new Date(instancia.virtual.dataTerminoProva)
  const status = instancia.status

  if (dataTerminoProva < agora || status === 'Concluída') {
    return instancia
  }
}
