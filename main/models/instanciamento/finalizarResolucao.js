import { InternalError, ForbiddenError } from 'main/erros'
import { enumStatusInstanciamento } from 'entidades/instanciamento/enumStatusInstanciamento'
import { errorCodes } from 'entidades/errorCodes'
import { inicioDoPeriodoDeAplicacaoJaPassou } from 'entidades/instanciamento'
import { atualizarNotasGrupos, atualizaStatusMatriz } from 'main/models/usuario'

const _rejectErro = (texto, codigo) => {
  let err
  err = new ForbiddenError(texto)
  err.code = codigo
  return Promise.reject(err)
}

const _instanciaJaConcluida = instancia => {
  return !!instancia.virtual.dataTerminouResolucao || instancia.status === enumStatusInstanciamento.concluida
}

export const finalizarResolucao = async instancia => {
  if (_instanciaJaConcluida(instancia)) {
    return _rejectErro('instancia de prova já foi concluída', errorCodes.instanciaJaConcluida)
  }
  if (!inicioDoPeriodoDeAplicacaoJaPassou(instancia)) {
    return _rejectErro('instancia de prova já foi concluída', errorCodes.foraDoPeriodoDeAplicacao)
  }
  const dataTerminouResolucao = new Date().toISOString()

  atualizarNotasGrupos(instancia)

  try {
    await instancia.updateAttributes(
      {
        status: enumStatusInstanciamento.concluida,
        virtual: { ...instancia.virtual, dataTerminouResolucao },
        prova: { ...instancia.prova, grupos: instancia.prova.grupos },
      },
      (err, instance) => {
        if (err) console.warn(err)
      }
    )
    await atualizaStatusMatriz(instancia)
    return { dataTerminouResolucao }
  } catch (erros) {
    new InternalError(erros)
  }
}
