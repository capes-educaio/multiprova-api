import { validarContratoComThrow, enviarParaGerarPreview } from 'main/helpers'
import { contratoQuestaoPreview } from 'entidades/questao/questaoPreview'
import { tiposTemplateQuestao } from 'entidades/questao/enumTipoTemplateQuestao'
import { ValidationError } from 'main/erros'

export const gerarPreviewQuestao = (dados, tipoTemplate, options) => {
  let template
  validarContratoComThrow({ contrato: contratoQuestaoPreview, dados })
  if (!tipoTemplate) tipoTemplate = 'geral'
  if (tiposTemplateQuestao[tipoTemplate]) template = tiposTemplateQuestao[tipoTemplate][dados.tipo]
  if (!template) throw new ValidationError('Template não encontrado: ', `${tipoTemplate}/${dados.tipo}`)
  const usuarioId = options.accessToken.userId
  const tituloImpressao = 'preview'
  return enviarParaGerarPreview({
    dadosParaInstanciar: dados,
    template,
    tituloImpressao,
    usuarioId,
    extensao: 'pdf',
    contentType: 'application/pdf',
    informacoes: { tipoDados: 'questao' },
  })
}
