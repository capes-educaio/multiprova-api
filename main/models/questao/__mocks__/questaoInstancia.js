export const questaoInstancia = {
  questaoMatrizId: 'e69ec34a-7226-40bc-aea1-0a8bc96399a6',
  numeroNaMatriz: 1,
  numeroNaInstancia: 1,
  fixa: false,
  valor: 10,
  enunciado:
    '<p>Sobre a mitocôndria: Foi descrita por Altmann, em 1894 (que as denominou &quot;bioblastos&quot;), sugerindo a sua relação com a oxidação celular. O seu número varia entre as células, sendo proporcional à atividade metabólica de cada uma, indo de quinhentas a mil ou até dez mil dessas estruturas por célula.</p>',
  tipo: 'vouf',
  vouf: {
    afirmacoes: [
      {
        texto: '<p>Verdade</p>',
        posicaoNaMatriz: 0,
        letra: 'V',
        respostaCandidato: 'V',
        dataRespostaCandidato: '2019-08-07T20:07:22.342Z',
      },
      {
        texto: '<p>Mentira</p>',
        posicaoNaMatriz: 2,
        letra: 'V',
        respostaCandidato: 'V',
        dataRespostaCandidato: '2019-08-07T20:07:21.306Z',
      },
      {
        texto: '<p>Falsidade</p>',
        posicaoNaMatriz: 3,
        letra: 'F',
        respostaCandidato: 'V',
        dataRespostaCandidato: '2019-08-07T20:07:23.791Z',
      },
      {
        texto: '<p>Falso</p>',
        posicaoNaMatriz: 1,
        letra: 'F',
        respostaCandidato: 'F',
        dataRespostaCandidato: '2019-08-07T20:07:25.178Z',
      },
    ],
  },
  notaQuestao: 7.5,
}
