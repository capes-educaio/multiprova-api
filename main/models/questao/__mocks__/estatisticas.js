export const estatisticas = {
  resumo: {
    acertos: 0.75,
    acertos10: 0.75,
    erros: 0.25,
    erros10: 0.25,
    numeroUsos: 11,
    somaNotasAlunos: 82.5,
    somaNotasAlunos10: 75,
    somaValorQuestoes: 110,
    somaValorQuestoes10: 100,
  },
  usos: [
    {
      acertos: 0.75,
      dataProva: '2019-08-02T00:00:00.342Z',
      erros: 0.25,
      idProva: '2be632a7-c117-4a49-aff7-73606a6147fe',
      somaNotasAlunos: 7.5,
      somaValorQuestoes: 10,
    },
    {
      acertos: 0.75,
      dataProva: '2019-08-02T00:00:00.342Z',
      erros: 0.25,
      idProva: '78f9e723-4bb2-40c4-9508-5b95d1751258',
      somaNotasAlunos: 7.5,
      somaValorQuestoes: 10,
    },
    {
      acertos: 0.75,
      dataProva: '2019-08-02T00:00:00.342Z',
      erros: 0.25,
      idProva: '159ba2df-71d5-48fa-877d-8ea300a32274',
      somaNotasAlunos: 7.5,
      somaValorQuestoes: 10,
    },
    {
      acertos: 0.75,
      dataProva: '2019-08-02T00:00:00.342Z',
      erros: 0.25,
      idProva: '2533edaf-9711-4662-81ca-36d6e74cd44e',
      somaNotasAlunos: 7.5,
      somaValorQuestoes: 10,
    },
    {
      acertos: 0.75,
      dataProva: '2019-08-02T00:00:00.342Z',
      erros: 0.25,
      idProva: '142308ee-a727-4b84-af70-d7536c9ce702',
      somaNotasAlunos: 7.5,
      somaValorQuestoes: 10,
    },
    {
      acertos: 0.75,
      dataProva: '2019-08-02T00:00:00.342Z',
      erros: 0.25,
      idProva: '84526fa5-1bf5-4dac-a555-c2e250f83a05',
      somaNotasAlunos: 7.5,
      somaValorQuestoes: 10,
    },
    {
      acertos: 0.75,
      dataProva: '2019-08-02T00:00:00.342Z',
      erros: 0.25,
      idProva: '99740d78-3f9a-498e-8293-602b8d424731',
      somaNotasAlunos: 7.5,
      somaValorQuestoes: 10,
    },
    {
      acertos: 0.75,
      dataProva: '2019-08-02T00:00:00.342Z',
      erros: 0.25,
      idProva: 'e79533c6-76cc-4e1b-9f58-f13f592f4e6f',
      somaNotasAlunos: 7.5,
      somaValorQuestoes: 10,
    },
    {
      acertos: 0.75,
      dataProva: '2019-08-02T00:00:00.342Z',
      erros: 0.25,
      idProva: '3f465e48-3321-440c-aae0-218f04fcfc2d',
      somaNotasAlunos: 7.5,
      somaValorQuestoes: 10,
    },
    {
      acertos: 0.75,
      dataProva: '2019-08-02T00:00:00.342Z',
      erros: 0.25,
      idProva: '3edefd8f-3d2e-4810-943a-3da317f43eb8',
      somaNotasAlunos: 7.5,
      somaValorQuestoes: 10,
    },
    {
      acertos: 0.75,
      dataProva: '2019-08-02T00:00:00.342Z',
      erros: 0.25,
      idProva: 'f7a6504d-0e80-4179-93d1-0cb57bda0b5f',
      somaNotasAlunos: 7.5,
      somaValorQuestoes: 10,
    },
  ],
}
