export const metodosParaDesativar = {
  standard: [
    'create',
    'replaceOrCreate',
    'patchOrCreate',
    'exists',
    'findById',
    'find',
    'findOne',
    'count',
    'replaceById',
    'createChangeStream',
    'updateAll',
    'replaceOrCreate',
    'replaceById',
    'upsertWithWhere',
  ],
}
