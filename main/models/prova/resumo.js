import app from 'server/server'
import { calcNotaProva } from './calcNotaProva'

export const resumo = async provaId => {
  const { Instanciamento } = app.models

  const instancias = await Instanciamento.find({ where: { idMatriz: provaId }, order: ['prova.nomeCandidato DESC'] })
  const resumoProva = await Promise.all(
    instancias.map(async ({ id, prova = {}, status }) => {
      const { titulo, grupos = [], nomeCandidato: nome, matricula, idNumero } = prova
      const [{ questoes }] = grupos
      return {
        id,
        idNumero,
        matricula,
        nome,
        titulo,
        status,
        nota: await calcNotaProva(questoes),
      }
    })
  )

  return [{ resumoProva: ordernarPorNome(resumoProva) }]
}

export const ordernarPorNome = array => {
  array.sort((a, b) => a.nome.localeCompare(b.nome, undefined, { ignorePunctuation: true }))
  return array
}
