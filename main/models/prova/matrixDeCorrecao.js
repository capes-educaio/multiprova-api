import app from 'server/server'
import { ordernarPorNome } from './resumo'
import { enumTipoProva } from 'entidades/enumTipoProva'

export const matrixDeCorrecao = async provaId => {
  const { Instanciamento, Prova } = app.models

  const instancias = await Instanciamento.find({ where: { idMatriz: provaId }, order: ['prova.nomeCandidato DESC'] })
  const { questoes: questoesDaProva = [], dadosAplicacao = {} } = (await Prova.findById(provaId)).toObject()
  const dataTerminoProva = dadosAplicacao.virtual ? new Date(dadosAplicacao.virtual.dataTerminoProva) : new Date(dadosAplicacao.papel.dataTerminoProva)
  const agora = new Date()

  let matrixDeCorrecao = instancias.map(({ id, prova = {}, status, notasPublicadas = false }) => {
    const { titulo, grupos = [], nomeCandidato: nome, matricula, idNumero, aplicacaoFinalizada = agora >= dataTerminoProva } = prova
    const [{ questoes }] = grupos
    return {
      id,
      idNumero,
      matricula,
      nome,
      titulo,
      status,
      notasPublicadas,
      aplicacaoFinalizada,
      questoes: questoes.map(({ observacao = '<p></p>', notaQuestao, ...questao, discursiva }) => {
        if (discursiva !== undefined) {
          if (prova.tipoProva === enumTipoProva.convencional) {
            const questaoMatriz = questoesDaProva.find(({ id }) => id === questao.questaoMatrizId)
            return { expectativaDeResposta: questaoMatriz.discursiva.expectativaDeResposta, observacao, notaQuestao, ...questao }
          }
          else {
            return { expectativaDeResposta: '<p></p>', observacao, notaQuestao, ...questao }
          }
        } else
          return { observacao, notaQuestao, ...questao }
      }),
    }
  })

  return [{ matrixDeCorrecao: ordernarPorNome(matrixDeCorrecao) }]
}
