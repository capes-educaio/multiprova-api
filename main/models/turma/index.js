export { registrarAlunos } from './registrarAlunos'
export { metodosParaDesativar } from './metodosParaDesativar'
export { getTurmasRecentes } from './getTurmasRecentes'
export { updateValidacaoAlunoTurmaByEmail } from './updateValidacaoAlunoTurmaByEmail'
