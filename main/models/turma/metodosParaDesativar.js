export const metodosParaDesativar = {
  standard: [
    'replaceOrCreate',
    'patchOrCreate',
    'exists',
    'findOne',
    'createChangeStream',
    'updateAll',
    'upsertWithWhere',
  ],
}
