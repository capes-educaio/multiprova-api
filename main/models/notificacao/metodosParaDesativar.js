export const metodosParaDesativar = {
  standard: [
    'replaceOrCreate',
    'patchOrCreate',
    'exists',
    'findOne',
    'find',
    'createChangeStream',
    'updateAll',
    'replaceOrCreate',
    'create',
    'upsertWithWhere',
    'count',
    'replaceById',
  ],
}
