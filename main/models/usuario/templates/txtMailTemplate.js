export const txtForgotEmail = ({ email, link }) => {
  return `Modificando senha
  Olá, esses são seus dados para efetuar seu login: 
  
  E-mail: ${email}

  Para continuar com a recuperação de senha, utilize o endereço "${link}" no navegador
    
  Atenciosamente,
  Equipe EducaIO`
}
