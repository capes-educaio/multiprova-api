import { enumStatusAplicacao } from 'entidades/enumStatusAplicacao'
import { enumStatusInstanciamento } from 'entidades/instanciamento/enumStatusInstanciamento'
import { enumTipoAplicacao } from 'entidades/enumTipoAplicacao'
import { enumTipoQuestao } from 'entidades/enumTipoQuestao'
import { models } from 'server/server'

const _janelaFechou = dataTerminoProva => {
  let time = new Date().toISOString()
  let dataTerminoProvaTz = new Date(dataTerminoProva).toISOString()
  let resultado = Date.parse(dataTerminoProvaTz) - Date.parse(time)
  return resultado <= 0 ? true : false
}

const _provaPodeMudarStatus = prova => {
  switch (prova.status) {
    case enumStatusAplicacao.emAplicacao:
      return true
    case enumStatusAplicacao.prontaParaAplicacao:
      return true
    default:
      return false
  }
}

const _temDadosAplicacao = prova => {
  return prova.hasOwnProperty('dadosAplicacao')
}

const _getData = prova => {
  switch (prova.dadosAplicacao.tipoAplicacao) {
    case enumTipoAplicacao.virtual:
      return {
        dataInicioProva: prova.dadosAplicacao.virtual.dataInicioProva,
        dataTerminoProva: prova.dadosAplicacao.virtual.dataTerminoProva,
      }
    case enumTipoAplicacao.papel:
      return {
        dataInicioProva: prova.dadosAplicacao.papel.dataAplicacao,
        dataTerminoProva: prova.dadosAplicacao.papel.dataTerminoProva,
      }
    default:
      return undefined
  }
}

const _deveZerarQuestaoDiscursiva = questao => {
  const isDiscursiva = questao.tipo === enumTipoQuestao.discursiva
  if (!questao || !isDiscursiva) return true
  if (isDiscursiva && !questao.discursiva.respostaCandidato) return true
  return false
}

const _atualizarNotas = questoes => {
  for (let questao of questoes) {
    switch (questao.tipo) {
      case enumTipoQuestao.bloco: {
        _atualizarNotas(questao.bloco.questoes)
        break
      }
      case enumTipoQuestao.discursiva: {
        if (_deveZerarQuestaoDiscursiva(questao)) {
          if (questao.notaQuestao === undefined) questao.notaQuestao = 0
        }
        break
      }
      case enumTipoQuestao.redacao: {
        break
      }
      case enumTipoQuestao.multiplaEscolha:
      case enumTipoQuestao.vouf:
      case enumTipoQuestao.associacaoDeColunas:
      default: {
        if (questao.notaQuestao === undefined) questao.notaQuestao = 0
      }
    }
  }
}

export const atualizarNotasGrupos = instancia => {
  if (!instancia && !instancia.prova && !instancia.prova.grupos) return
  instancia.prova.grupos.forEach(grupo => {
    _atualizarNotas(grupo.questoes)
  })
}

const _atualizaStatusDaProva = prova => {
  const { Prova, Instanciamento } = models
  Prova.findById(prova.id).then(prova => prova && prova.updateAttributes({ status: enumStatusAplicacao.aplicada }))
  Instanciamento.find({ where: { idMatriz: prova.id, isDeleted: false } }, (err, instancias) => {
    for (let instancia of instancias) {
      if (instancia.status === enumStatusInstanciamento.emExecucao) {
        // eslint-disable-next-line default-case
        switch (instancia.tipoAplicacao) {
          case enumTipoAplicacao.virtual:
            instancia.virtual.dataTerminouResolucao = new Date().toISOString()
            break
          case enumTipoAplicacao.papel:
            instancia.papel.dataTerminouResolucao = new Date().toISOString()
            break
        }
        instancia.status = enumStatusInstanciamento.concluida
      } else if (instancia.status === enumStatusInstanciamento.naoIniciada) {
        atualizarNotasGrupos(instancia)
      }
      instancia.save()
    }
  })
}

const _janelaDeTempoJaAbriu = (dataInicioProva, dataTerminoProva) => {
  let timestampAtual = new Date()
  let dataTerminoProvaTz = new Date(dataTerminoProva)
  let dataInicioProvaTz = new Date(dataInicioProva)
  let resultado = timestampAtual >= dataInicioProvaTz && timestampAtual <= dataTerminoProvaTz
  return resultado
}

const _mudaStatusParaEmAplicacao = async prova => {
  const { Prova } = models
  await Prova.findById(prova.id).then(
    prova => prova && prova.updateAttributes({ status: enumStatusAplicacao.emAplicacao })
  )
}

const _verificaJanelaDeAplicacao = prova => {
  const validaStatus = _provaPodeMudarStatus(prova)
  const validaDadosAplicacao = _temDadosAplicacao(prova)
  if (validaStatus === true && validaDadosAplicacao === true) {
    const { dataInicioProva, dataTerminoProva } = _getData(prova)
    if (_janelaFechou(dataTerminoProva)) _atualizaStatusDaProva(prova)
    else if (_janelaDeTempoJaAbriu(dataInicioProva, dataTerminoProva)) {
      _mudaStatusParaEmAplicacao(prova)
    }
  }
}

export const atualizaStatusMatriz = async instancia => {
  if (!instancia) return
  const { Prova } = models
  const prova = await Prova.findById(instancia.idMatriz)
  _verificaJanelaDeAplicacao(prova)
}

export const atualizaStatusProvas = async contexto => {
  contexto.result.forEach(_verificaJanelaDeAplicacao)
}
