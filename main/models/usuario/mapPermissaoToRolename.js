export const mapPermissionToRolename = {
  1: 'admin',
  2: 'docente',
  3: 'discente',
  4: 'super',
  5: 'gestor',
}
