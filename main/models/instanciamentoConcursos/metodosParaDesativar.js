export const metodosParaDesativar = {
  standard: [
    'create',
    'replaceOrCreate',
    'patchOrCreate',
    'deleteById',
    'exists',
    'findOne',
    'replaceById',
    'createChangeStream',
    'updateAll',
    'replaceOrCreate',
    'replaceById',
    'upsertWithWhere',
    'prototype.patchAttributes',
  ],
}
