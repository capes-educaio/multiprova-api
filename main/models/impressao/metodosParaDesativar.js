export const metodosParaDesativar = {
  standard: [
    'prototype.patchAttributes',
    'patchOrCreate',
    'create',
    'replaceOrCreate',
    'upsertWithWhere',
    'deleteById',
    'updateAll',
    'find',
    'findOne',
    'deleteById',
    'replaceById',
    'count',
    'exists',
    'createChangeStream',
  ],
}
